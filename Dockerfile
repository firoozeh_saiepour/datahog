FROM node:12.18.0

WORKDIR /usr/src

COPY package*.json .babelrc ./

RUN npm install

COPY ./src ./src
RUN npm run build

COPY ./src/providers.json ./services
COPY ./api-docs ./api-docs
