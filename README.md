# Datahog Webhook Solution

I moved the original README document to: [requirements.md](requirements.md)

## Running the solution locally
To build and start all the required services please run this in the terminal:
```bash
docker-compose up --build
```
and emulate a request from a consumer application by running this:
```bash
curl --location --request POST 'http://localhost:8081/request' \
--header 'Content-Type: application/json' \
--data-raw '{
	"providers": [ "internet", "gas"],
	"callback_url": "http://consumer:3002/response_callback"
}'
```
The POST request above would register a request to retrieve the bill information from `internet` and `gas` providers into `http://consumer:3002/response_callback` callback url once the data is collected.

Once the `consumer` services receives the requested data we would be able to see this message from the `consumer container` in the terminal:
```
consumer              | Consume Data from provider: {"data":{"internet":[{"billedOn":"2020-02-07T15:03:14.257Z","amount":"15.12"},{"billedOn":"2020-03-07T15:03:14.257Z","amount":"15.12"}],"gas":[{"billedOn":"2020-04-07T15:03:14.257Z","amount":"22.27"},{"billedOn":"2020-05-07T15:03:14.257Z","amount":"30"}]}}
``` 


---

# Architecture Decision Record

## Context
We would like to build a scalable and fault tolerant solution in order to provide access to a third party API `Datahog` from our consumer applications.

We have experienced various issues with this API, such as long scheduled maintenanace, temporary bad gateway errors due to load etc...

In the other hand our consumer applications are capable of making a REST API call to request the data and consume the collected data in an asynchronous manner. 

## Desicion
we have decided to build a webhook-based API, which accepts a POST request payload with the list of providers to collect the data from ("gas" or "internet"), and a publicly accessible endpoint to call back, once the data for the requested providers are collected.

To make our solution both scalable and fault tolerant we've decided to decouple the request registeration from request execution.

![Datahog system design](wonderbill_datahog.png)

### Gateway Service
In order to implement the request registeration, we've decided to build a `gateway` service to accept the requests from consumers by providing a REST API with this specification: http://localhost:8081/api-docs/#/default/post_request

The `gateway` services records the request information (including required providers, callback_url and the time of the request) in a `redis` cache.

As this service would be our only public facing service in a productionised solution we need to consider security for this API.

### Request Processor Service
In order to process the requests from consumers we've decided to build a `request_processor` service.

This service polls all the outstanding requests from the `redis` cache in managable intervals (currently every 5 seconds) and processes the requests.

### GraphQL Proxy Service - Datahog Aggregator
We also have decided to build a GraphQL service (`datahog_aggregator`) as a proxy to enable fetching multiple providers data in one request and aggregate the retrieved data. http://localhost:8084/graphql

---

### Request Processing Strategy
When the consumer registers their request through `gateway` service, we record the request with these entries in the `redis` cache:
```json
{
    "request_id":"req_d381503e-da0d-48f9-84a7-0d90a9b06209",
    "callback_url":"http://consumer:3002/response_callback",
    "providers":["internet","gas"],
    "retry_count":0,
    "last_try":"2020-06-08T10:04:33+00:00"
}
```
We add a `request_id` in order to identify the request and for cleanup purposes, a `retry_count` that demonestrates the number of attempts we've made to retrieve the data and forward to the callback url and also `last_try` represents the last time we attempted to processes this request (whether failed or succeeded).

In every attempt to process the request we update the `last_try` to the current date and time and increase the `retry_count` by one.

To consider the process successful we should be able to:

 - Retrieve all the requested providers data without any errors or partialy failed responses
 - Successfully forward the data to the callback_url

Once we acheived all the requirements above (our attempt to process is successful) we can remove the request from the `redis` cache. But in the case of a failure (retrieval or forward), we don't take any action.

The `request_processor` uses an `exponential backoff strategy` to allow time to recover and avoid overloading the 3rd party API (and potentially contribute to the downtime). We calculate the end of the backoff delay by this simple calculation inside the `can_process` function:
```javascript
var next_retry = moment(last_try).add(backoff_delay_factor * Math.pow(2,retry_count),'s')
return (current_time > next_retry);
```

## Consequences
### Scalability
As we've separated the request `registeration` and `processing` and we also built our services as stateless, we are able to increase the number of `gateway` and `request_processor` services without any change to the current code or the architectural design and any outstanding request in the `redis` cache that is due for a process attempt can be picked up by any instance of `request_processor`. 

### Fault tolerance
All of the services in this design are dockerised and stateless and not acpected to be long lived. So down time in these services (`gateway`, `request_processor`) or failures won't cause any issue other than more delay in responsing to the consumer. Once the services restart, the process can continue.

### Service Level Agreement consideration
In the best scenario when all our services are functioning, we might have a delay up to 5 second to respond to the consumer. This is due to the duration of the processing interval which is set to 5 seconds.

### Known duplicate callback issues
- As we are using one single `redis` cache, if we scale up and use multiple instances of `request_processor`, in very rare cases the same `request` might get picked up by multiple `request_processors` and we might end up with forwarding to the same callback_url multiple times. One hacky but very simple solution to `reduce` this risk would be to check the `can_process` again after a very short `random` delay and update the request only after that second check passes.
- Another issue is when the same `consumer` makes the same request multiple times. Assuming we are always only interested in the latest data from the 3rd party APIs,  this can easily be prevented in the `registration phase` by deduplicating the requests before adding them to the `redis` cache again.

### Consider reporting downtime
We might want to stop retrying after a certian number of atempts and report down time. This depends to the business requirements.

### 🙏🙏THANK YOU FOR YOUR TIME AND PATIENCE! 🙏🙏 ###









