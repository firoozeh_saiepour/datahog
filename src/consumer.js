import express from 'express';
import bodyParser from 'body-parser';

const app = express();
const port = 3002;

app.use(bodyParser.json({ extended: true }));

app.post('/response_callback', (req, res) => {
    consume_data(req.body);
    res.sendStatus(200);
});

function consume_data(data){
    console.log(`Consume Data from provider: ${JSON.stringify(data)}`)
}

app.listen(port, () => console.log(`Consumer server listening at http://localhost:${port}`));
export default app;
