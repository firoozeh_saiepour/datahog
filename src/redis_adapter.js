const redis  = require('redis')
const redis_client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_URL)

export default redis_client