import http from 'http';
import moment from'moment';
import redis_client  from './redis_adapter.js';

const backoff_delay_factor = 2;

redis_client.on('connect', function() {
    console.log('Redis Connected...');
});

function retrieve_options(len) {
    return {
      host: process.env.DATAHOG_AGGREGATOR_URL,
      path: `/graphql`,
      port: process.env.DATAHOG_AGGREGATOR_PORT,
      method: 'GET',
      timeout: 5000,
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': len
      }
    }
}

function forward_options(callback_url, len) {
    var url = new URL(callback_url);
    return {
      host: url.hostname,
      path: url.pathname,
      port: url.port,
      method: 'POST',
      timeout: 5000,
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': len
      }
    }
}
  
function poll_requests(){
    redis_client.keys('req_*', function (err, keys) {
        if (err) return console.log(err);
        console.log(`Process ${keys.length} outstanding request(s):`)

        for(let i=0;i<keys.length;i++){
            redis_client.get(keys[i], (err, reply)=>{
                process_request(reply);
            })
        }
      });   
}

function process_request(request){
    var request = JSON.parse(request);

    if(!can_process(request)) return;

    const request_id =  request.request_id;

    request.last_try = moment().format();
    request.retry_count = request.retry_count + 1;

    //Set the retry information
    redis_client.set(request_id, JSON.stringify(request));

    //Try processing the request
    try_to_process(request);

    console.log(`processing ${request.request_id} request: ${JSON.stringify(request)}`);
}

function can_process(request){
    const last_try = request.last_try;
    const retry_count = request.retry_count;

    var current_time = moment(Date.now());
    // Exponential backoff strategy
    var next_retry = moment(last_try).add(backoff_delay_factor * Math.pow(2,retry_count),'s')

    return (current_time > next_retry);
}

function try_to_process(request_data){
    let strQuery = "";
    for(let i in request_data.providers){
        strQuery += `${request_data.providers[i]} { billedOn, amount } `;
    }
    let query = `{"query":"{${strQuery}}"}`
    console.log(query);

    const request = http.request(retrieve_options(query.length), function(response) {
        console.log(`retrieve statusCode: ${response.statusCode}`)
        if(response.statusCode!='200') {
            request.abort;
            return;
        }

        var retrieved_data = '';
        //another chunk of data has been received, so append it to `str`
        response.on('data', function (chunk) {
            retrieved_data += chunk;
        });

        //the whole response has been received, so we just print it out here
        response.on('end', function () {
            const retrieved_data_json = JSON.parse(retrieved_data)
            if (retrieved_data_json.errors) {
                console.log(`Datahog aggregator failed to retrieve all the request data: ${retrieved_data_json.errors[0].message}`);
            } else {
                console.log(`Forward retrieved data: ${retrieved_data} for request: ${JSON.stringify(request_data)}`)
                forward(retrieved_data, request_data);
            }
        });  

    });

    request.on('timeout', (err) => {
        console.log(`Request timed out: ${err}`);
        request.abort();
    });

    request.on('error', (err) => {
        console.log(`Request failed: ${err}`);
        request.abort();
    });
    request.write(query);
    request.end();
}

function forward(retrieved_data, request_body){
    const req = http.request(forward_options(request_body.callback_url, retrieved_data.length), (res) => {
        console.log(`forward statusCode: ${res.statusCode}`)
        if(res.statusCode=='200') {
            //Data is forwarded successfully
            cleanup_request(request_body.request_id);
        } else {
            req.abort;
            return;
        }
      
        res.on('data', (d) => {
          console.log(`forward data received ${d}`);
        })
      })
      
      req.on('error', (error) => {
        console.error(`error in forwarding ${error}`)
      })
      req.write(retrieved_data)
      req.end()
}

function cleanup_request(request_id){
    redis_client.del(request_id);
    console.log(`request_id ${request_id} deleted from the cache.`)
}

var app = setInterval(() => {
    poll_requests();
}, 5000);

export default app;