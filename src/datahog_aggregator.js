import express from 'express';
import fetch from 'node-fetch';
import graphqlHTTP from 'express-graphql';
import { buildSchema } from 'graphql';

const port = 3004;

// Construct a schema, using GraphQL schema language
const schema = buildSchema(`
type Bill {
  billedOn: String!,
  amount: String!
}
type Query {
    gas: [Bill],
    internet: [Bill],
    water: [Bill]
  }
`);

function checkStatus(res) {
  if (res.ok) { // res.status >= 200 && res.status < 300
      return res;
  } else {
      throw (res.statusText);
  }
}

// The root provides a resolver function for each API endpoint
const root = {
  gas: async () => {
    let data = await fetch(`http://${process.env.DATAHOG_API_URL}:${process.env.DATAHOG_API_PORT}/providers/gas`)
      .then(res => checkStatus(res))
      .then(res => res.json());
    return data;
  },
  internet: async () => {
    let data = await fetch(`http://${process.env.DATAHOG_API_URL}:${process.env.DATAHOG_API_PORT}/providers/internet`)
    .then(res => checkStatus(res))
    .then(res => res.json());
    return data;
  },
  water: async () => {
    let data = await fetch(`http://${process.env.DATAHOG_API_URL}:${process.env.DATAHOG_API_PORT}/providers/water`)
    .then(res => checkStatus(res))
    .then(res => res.json());
    return data;
  }
};

const app = express();
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));

app.listen(port, () => console.log(`GraphQL server listening at http://localhost:${port}`));

export default app;