import app from './gateway'

const port = 3001;

app.listen(port, () => console.log(`Gateway server listening at http://localhost:${port}`));