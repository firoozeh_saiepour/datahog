import express from 'express'
import bodyParser from 'body-parser';

import swaggerUi from 'swagger-ui-express';
import YAML from 'yamljs';

import redis_client  from './redis_adapter.js';

import { v4 as uuidv4 } from 'uuid';
import moment from 'moment';
import validUrl from 'valid-url';

const swaggerDocument = YAML.load('api-docs/gateway.yaml');

const app = express();

redis_client.on('connect', function() {
  console.log('Redis Connected...');
});

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(bodyParser.json({ extended: true }));
app.use(function (error, req, res, next) {
  if(error instanceof SyntaxError){ //Handle SyntaxError here.
    return res.status(400).send({data : "Invalid data"});
  } else {
    next();
  }
});

app.post('/request', (req, res) => {
    if(!validate_request(req)){
      res.statusCode = 400
      return res.status(400).send({data : "Invalid data"});
    }
    const request_id = `req_${uuidv4()}`
    const request = {
      "request_id": request_id,
      "callback_url": req.body.callback_url, 
      "providers": req.body.providers,
      "retry_count": 0,
      "last_try": moment().format()
    }

    const results = register_request(request);
    if(results){
      res.status(201).send({data : "Request Registered"});
    } else {
      return res.status(500).send({data : "Registering the request failed."});
    }
});

async function register_request(request){
  return await redis_client.set(request.request_id, JSON.stringify(request), function(err, reply){
    if(err){
      console.log('Registering the request failed')
      return false;
    }
    if(reply){
      console.log(`Request ${request.request_id} registered with: ${JSON.stringify(request)}`)
      return true;
    }
  });
}

function validate_request(request){
  const valid_providers = ["gas", "internet", "water"];
  if(request.body.providers == null || request.body.providers.length == 0) return false;
  if(request.body.callback_url == null) return false;
  for(let i in request.body.providers){
    if(!valid_providers.includes(request.body.providers[i])) return false;
  }
  if (!validUrl.isUri(request.body.callback_url)) return false;

  return true;
}

module.exports = app