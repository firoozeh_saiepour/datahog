require('../src/redis_adapter')

const app = require('../src/gateway');
const supertest = require('supertest');

jest.mock('../src/redis_adapter', () => ({
  on: () => { console.log('mock redis connected');return; },
  set: (err, replay) => { return; }
}));

describe('POST /request', ()=> {
  it('returns status code 400 when providers is missing from the request', done =>{
    supertest(app)
    .post("/request")
    .send({
      "callback_url": "http://consumer:3002/response_callback"
    })
    .expect(400)
    .end(done)
  })

  it('returns status code 400 when callback_url is missing from the request', done =>{
    supertest(app)
    .post("/request")
    .send({
      "providers": [ "internet", "gas"]
    })
    .expect(400)
    .end(done)
  })
  
  it('returns status code 400 when there is an invalid provider', done =>{
    supertest(app)
    .post("/request")
    .send({
      "providers": [ "internet", "gas2"],
      "callback_url": "http://consumer:3002/response_callback"
    })
    .expect(400)
    .end(done)
  })

  it('returns status code 201 when the request body is correct', done =>{
    supertest(app)
    .post("/request")
    .send({
      "providers": [ "internet", "gas"],
      "callback_url": "http://consumer:3002/response_callback"
    })
    .expect(201)
    .end(done)
  })
})
